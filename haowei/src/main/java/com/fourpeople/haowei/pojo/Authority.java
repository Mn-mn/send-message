package com.fourpeople.haowei.pojo;

public class Authority {
    /**
     *
     */
    private Integer aid;

    /**
     *
     */
    private String authorityname;

    /**
     *
     */
    private String url;

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public String getAuthorityname() {
        return authorityname;
    }

    public void setAuthorityname(String authorityname) {
        this.authorityname = authorityname;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

