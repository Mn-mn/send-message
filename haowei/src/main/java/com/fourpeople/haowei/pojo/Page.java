package com.fourpeople.haowei.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Page {
    private List<Authority> data;
    private int total;

}
