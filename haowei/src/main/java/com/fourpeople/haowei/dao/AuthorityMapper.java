package com.fourpeople.haowei.dao;

import com.fourpeople.haowei.pojo.Authority;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorityMapper {
    List<Authority> AllAuthority(Integer pageNo,Integer pegeSize);
    int AuthorityCount();
    Authority AuthorityById(int id);
    int UpdateAuthority(int id);
    int AddAuthority(Authority authority);
    int DeleteAuthority(int id);

}
