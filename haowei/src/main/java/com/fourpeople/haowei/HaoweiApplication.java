package com.fourpeople.haowei;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.fourpeople.haowei.dao")
public class HaoweiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HaoweiApplication.class, args);
    }

}
