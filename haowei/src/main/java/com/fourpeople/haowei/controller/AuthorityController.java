package com.fourpeople.haowei.controller;


import com.fourpeople.haowei.pojo.Page;
import com.fourpeople.haowei.service.AuthorityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AuthorityController {

    @Autowired
    AuthorityServiceImpl authorityServiceImpl;

    @PostMapping("/login/{pageNo}/{pageSize}")
    public Page login(@PathVariable String pageNo, @PathVariable String pageSize){
        return authorityServiceImpl.AllAuthority(Integer.parseInt(pageNo),Integer.parseInt(pageSize));
    }

}
