package com.fourpeople.haowei.service;

import com.fourpeople.haowei.pojo.Authority;
import com.fourpeople.haowei.pojo.Page;



public interface AuthorityService {
    Page AllAuthority(Integer pageNo, Integer pageSize);
    Authority AuthorityById(int id);
    int UpdateAuthority(int id);
    int AddAuthority(Authority authority);
    int DeleteAuthority(int id);
}
