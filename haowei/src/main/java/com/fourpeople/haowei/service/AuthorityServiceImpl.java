package com.fourpeople.haowei.service;


import com.fourpeople.haowei.dao.AuthorityMapper;
import com.fourpeople.haowei.pojo.Authority;
import com.fourpeople.haowei.pojo.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityServiceImpl implements AuthorityService{

    @Autowired
    AuthorityMapper authorityMapper;

    @Override
    public Page AllAuthority(Integer pageNo,Integer pageSize) {

        if(pageNo!=null&& pageSize!=null){
            pageNo=(pageNo-1)*pageSize;
        }
        List<Authority> data=authorityMapper.AllAuthority(pageNo,pageSize);
        int total=authorityMapper.AuthorityCount();
        Page page = new Page();
        page.setData(data);
        page.setTotal(total);
       return page;
    }






    @Override
    public Authority AuthorityById(int id) {
        return authorityMapper.AuthorityById(id);
    }

    @Override
    public int UpdateAuthority(int id) {
        return authorityMapper.UpdateAuthority(id);
    }

    @Override
    public int AddAuthority(Authority authority) {
        return authorityMapper.AddAuthority(authority);
    }

    @Override
    public int DeleteAuthority(int id) {
        return authorityMapper.DeleteAuthority(id);
    }
}
