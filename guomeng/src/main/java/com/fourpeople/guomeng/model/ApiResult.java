package com.fourpeople.guomeng.model;

public class ApiResult<T> {
   private int code;

   private String message;

   private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> ApiResult<T> success(T data){
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(ResultMsgEnum.SUCCESS.getCode());
        apiResult.setMessage(ResultMsgEnum.SUCCESS.getMessage());
        apiResult.setData(data);
        return apiResult;
    }

    public static <T> ApiResult<T> erro(int code,String message){
        ApiResult<T> apiResult = new ApiResult<>();
        apiResult.setCode(code);
        apiResult.setMessage(message);
        return apiResult;
    }
}
