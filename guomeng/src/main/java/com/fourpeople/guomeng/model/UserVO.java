package com.fourpeople.guomeng.model;

import com.fourpeople.guomeng.entity.User;

import java.util.List;

public class UserVO {

    private User user;

    List<String> urlList;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getUrlList() {
        return urlList;
    }

    public void setUrlList(List<String> urlList) {
        this.urlList = urlList;
    }
}
