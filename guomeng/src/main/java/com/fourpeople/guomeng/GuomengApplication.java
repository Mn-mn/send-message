package com.fourpeople.guomeng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan("com.fourpeople.guomeng.dao")
public class GuomengApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuomengApplication.class, args);
    }

}
