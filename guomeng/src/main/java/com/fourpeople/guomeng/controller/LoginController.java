package com.fourpeople.guomeng.controller;

import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.service.impl.LoginServiceImpl;
import com.fourpeople.guomeng.util.ResourceListVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "用户登录")
@RestController
@RequestMapping("/")
public class LoginController {

    @Autowired
    LoginServiceImpl loginServiceImpl;


    @ApiOperation(value = "登录", notes = "用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true),
            @ApiImplicitParam(name = "password", value = "登陆密码", required = true)
    })
    @PostMapping("/login")
    public User login(@RequestParam String username, @RequestParam String password) {
        return loginServiceImpl.login(username, password);
    }



    @ApiOperation(value = "菜单", notes = "查询用户菜单栏")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true),
            @ApiImplicitParam(name = "password", value = "登陆密码", required = true)
    })
    @PostMapping("/menu")
    public List<ResourceListVO> loginFindMenuTree(String username, String password) {
        System.out.println("name: "+username+"\n"+"pwd: "+password);
        return loginServiceImpl.queryMenuTree(username, password);
    }

}
