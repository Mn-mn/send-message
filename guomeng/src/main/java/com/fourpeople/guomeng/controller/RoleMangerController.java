package com.fourpeople.guomeng.controller;

import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.entity.Role;
import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.model.PageModel;
import com.fourpeople.guomeng.service.impl.PageServiceImpl;
import com.fourpeople.guomeng.service.impl.RoleMangerserviceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "角色操作")
@RestController
@RequestMapping("/role")
public class RoleMangerController {

    @Autowired
    RoleMangerserviceImpl roleMangerServiceImpl;

    @Autowired
    PageServiceImpl pageService;


    @ApiOperation(value = " 增加角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rolename",value = "角色名"),
    })
    @PostMapping("/add")
    public Integer addRole(Role role){
        return roleMangerServiceImpl.roleAdd(role);
    }

    @ApiOperation(value = " 删除角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rid" ,value = "角色id",required = true)
    })
    @PostMapping("/delete")
    public Integer deleteRole(Role role){
        return roleMangerServiceImpl.roleDelete(role);
    }

    @ApiOperation(value = " 更新角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rid" ,value = "角色id",required = true),
            @ApiImplicitParam(name = "rolename" ,value = "角色名",required = true)
    })
    @PostMapping("/update")
    public Integer updateRole(Role role){
        return roleMangerServiceImpl.roleUpdate(role);
    }

    @ApiOperation(value = " 查询角色通过用户id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid",value = "用户id",required = true),
            @ApiImplicitParam(name = "username",value = "用户名")
    })
    @PostMapping("/querybyuid")
    public Role queryByUid(User user){
        return roleMangerServiceImpl.roleByUid(user);
    }

    @ApiOperation(value = " 查询角色通过权限id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "aid",value = "权限id",required = true),
            @ApiImplicitParam(name = "authorityname",value = "权限名")
    })
    @PostMapping("/querybyaid")
    public List<Role> queryByAid(Authority authority){
        return roleMangerServiceImpl.roleByAid(authority);
    }

    @ApiOperation(value = " 查询所有角色")
    @GetMapping("/queryall")
    public List<Role> queryAll(){
        return roleMangerServiceImpl.roleAll();
    }



    @ApiOperation(value = "根据查询并带分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageSize",value = "每页记录数"),
            @ApiImplicitParam(name = "pageNo",value = "当前第几页"),
            @ApiImplicitParam(name = "rolename",value = "quanxianming")

    })
    @PostMapping("/page")
    public PageModel queryAllByPage(String pageSize,String pageNo,String rolename){
        return pageService.findRoleByPage(pageSize,pageNo,rolename);
    }
}
