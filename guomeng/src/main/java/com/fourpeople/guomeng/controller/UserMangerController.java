package com.fourpeople.guomeng.controller;

import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.model.UserVO;
import com.fourpeople.guomeng.service.impl.UserMangerServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "用户信息")
@RestController
@RequestMapping("/user")
public class UserMangerController {

    @Autowired
    UserMangerServiceImpl userMangerImpl;


    @ApiOperation(value = "查询所有用户信息")
    @GetMapping("/all")
    public List<UserVO> findAllUserMessage(){
        return userMangerImpl.userList();
    }

    @ApiOperation(value = "根据单个信息查询单个用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid",value = "用户id"),
            @ApiImplicitParam(name = "username",value = "用户名")
    })
    @GetMapping("/userbyone")
    public User findByOneUser(String uid,String username){
        User user = new User();
        user.setUserName(username);
        user.setUid(Integer.parseInt(uid));
        return userMangerImpl.userBySelcyive(user);
    }

    @ApiOperation(value = "根据用户名修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid",value = "用户id"),
            @ApiImplicitParam(name = "username",value = "用户名"),
            @ApiImplicitParam(name = "password",value = "密码"),
    })
    @PostMapping("/update")
    public Integer updateUser(String uid,String username,String password){
        User user = new User();
        user.setUid(Integer.parseInt(uid));
        user.setUserName(username);
        user.setPassWord(password);
        return userMangerImpl.updateUser(user);

    }




}
