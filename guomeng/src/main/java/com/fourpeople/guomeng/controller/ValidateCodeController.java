package com.fourpeople.guomeng.controller;


import com.fourpeople.guomeng.service.impl.ValidateCodeServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Api(tags = "验证码")
@RestController
@RequestMapping("/yanzhengma")
public class ValidateCodeController {

    @Autowired
    ValidateCodeServiceImpl validateCodeService;

    @ApiOperation(value = "得到验证码")
    @GetMapping("/get")
    public String getValidate(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        return validateCodeService.getRandomCodeBase64(httpServletRequest, httpServletResponse);
    }


    @GetMapping("/getcode")
    public void getSessionCode(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        System.out.println(httpServletRequest.getSession().getAttribute("sessionKey"));
    }


}
