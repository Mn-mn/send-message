package com.fourpeople.guomeng.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourpeople.guomeng.model.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;


/**
 * 注意点：在@RestControllerAdvice中要指定接口的包，否则会出现swagger的错误
 */
@RestControllerAdvice("com.fourpeople.guomeng.controller")
public class ResponseAdvice implements ResponseBodyAdvice<Object> {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 开启拦截
     * @param methodParameter
     * @param aClass
     * @return
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return true;
    }


    /**
     * 处理返回前的数据
     * @param o
     * @param methodParameter
     * @param mediaType
     * @param aClass
     * @param serverHttpRequest
     * @param serverHttpResponse
     * @return
     */
    @Override
    public Object beforeBodyWrite(Object o, MethodParameter methodParameter, MediaType mediaType, Class aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {


        /**
         * 必要加的判断，判断字符串类型,因为字符串类型不会自动转成json格式
         */
        if (o instanceof String) {
            try {





                return objectMapper.writeValueAsString(ApiResult.success(o));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }


        /**
         * 异常判断，因为异常已经成为ApiResult的形式，不需要再包装一层
         */
        if (o instanceof ApiResult) {
            return o;
        }

        return ApiResult.success(o);

    }
}