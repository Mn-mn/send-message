package com.fourpeople.guomeng.dao;

import com.fourpeople.guomeng.entity.UserRole;

import java.util.List;

public interface UserRoleMapper {
    int deleteByPrimaryKey(Integer urid);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Integer urid);

    UserRole selectOneByUid(Integer uid);

    List<UserRole> selectAllByRid(Integer rid);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);
}