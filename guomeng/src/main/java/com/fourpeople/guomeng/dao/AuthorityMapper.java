package com.fourpeople.guomeng.dao;

import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.model.PageModel;
import com.fourpeople.guomeng.model.QueryCondition;

import java.util.List;

public interface AuthorityMapper {
    int deleteByPrimaryKey(Integer aid);

    int insert(Authority record);

    int insertSelective(Authority record);

    Authority selectByPrimaryKey(Integer aid);

    List<Authority> selectByRid(Integer rid);

    int updateByPrimaryKeySelective(Authority record);

    int updateByPrimaryKey(Authority record);

    Integer totalCountByselectByQuerycondition(QueryCondition queryCondition);

    List<Authority> selectByQuerycondition(QueryCondition queryCondition, PageModel pageModel);
}