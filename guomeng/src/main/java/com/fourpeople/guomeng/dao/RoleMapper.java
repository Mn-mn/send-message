package com.fourpeople.guomeng.dao;

import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.entity.Role;
import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.model.PageModel;
import com.fourpeople.guomeng.model.QueryCondition;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer rid);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer rid);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    List<Role> selectAll();

    Role selectByUid(User user);

    List<Role> selectByAid(Authority authority);

    Integer totalCountByselectByQuerycondition(QueryCondition queryCondition);

    List<Role> selectByQuerycondition(QueryCondition queryCondition, PageModel<Role> rolePageModel);
}