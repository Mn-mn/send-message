package com.fourpeople.guomeng.dao;

import com.fourpeople.guomeng.entity.RoleAuthority;

import java.util.List;

public interface RoleAuthorityMapper {
    int deleteByPrimaryKey(Integer raid);

    int insert(RoleAuthority record);

    int insertSelective(RoleAuthority record);

    RoleAuthority selectByPrimaryKey(Integer raid);

    List<RoleAuthority> selectByRid(Integer rid);

    int updateByPrimaryKeySelective(RoleAuthority record);

    int updateByPrimaryKey(RoleAuthority record);
}