package com.fourpeople.guomeng.dao;

import com.fourpeople.guomeng.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface UserMapper {

    User findByUsername(User user);

    List<User> findByAll();

    Integer deleteByUsername(User user);

    Integer updateByUsername(User user);

    Integer insert(User user);

    User finbBySelective(User user);
}
