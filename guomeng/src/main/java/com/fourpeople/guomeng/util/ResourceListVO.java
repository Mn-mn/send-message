package com.fourpeople.guomeng.util;

import java.util.List;
import java.util.Objects;

public class ResourceListVO implements TreeNode<Integer> {
    private Integer id;

    private String name;

    private String url;

    private Integer parentid;

    private List<ResourceListVO> children;


    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getParentid() {
        return parentid;
    }

    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<ResourceListVO> getChildren() {
        return children;
    }


    @Override
    public Integer id() {
        return this.id;
    }

    @Override
    public Integer parentId() {
        return this.parentid;
    }

    @Override
    public boolean root() {
        return Objects.equals(this.parentid, 0);
    }

    @Override
    public void setChildren(List children) {
        this.children = children;
    }
}


