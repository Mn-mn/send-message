package com.fourpeople.guomeng.service.impl;

import com.fourpeople.guomeng.dao.AuthorityMapper;
import com.fourpeople.guomeng.dao.RoleMapper;
import com.fourpeople.guomeng.dao.UserMapper;
import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.entity.Role;
import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.model.UserVO;
import com.fourpeople.guomeng.service.UserMangerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserMangerServiceImpl implements UserMangerService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    AuthorityMapper authorityMapper;



    @Override
    public List<UserVO> userList() {
        List<User> userList = userMapper.findByAll();


        List<UserVO> collect = userList.stream().map(u -> {
            UserVO userVO = new UserVO();
            Role role = roleMapper.selectByUid(u);
            ArrayList<String> strings = new ArrayList<>();
            List<Authority> authorities = authorityMapper.selectByRid(role.getRid());
            for (Authority authority : authorities) {
                strings.add(authority.getAuthorityname());
            }
            userVO.setUser(u);
            userVO.setUrlList(strings);
            return userVO;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public User userBySelcyive(User user) {
        if (user!=null){
            return userMapper.finbBySelective(user);
        }
        return null;


    }

    @Override
    public Integer updateUser(User user) {
        return userMapper.updateByUsername(user);
    }


}
