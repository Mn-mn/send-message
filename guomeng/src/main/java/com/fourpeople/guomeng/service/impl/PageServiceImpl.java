package com.fourpeople.guomeng.service.impl;

import com.fourpeople.guomeng.dao.AuthorityMapper;
import com.fourpeople.guomeng.dao.RoleMapper;
import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.entity.Role;
import com.fourpeople.guomeng.model.PageModel;
import com.fourpeople.guomeng.model.QueryCondition;
import com.fourpeople.guomeng.service.PageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PageServiceImpl implements PageService {


    @Autowired
    AuthorityMapper authorityMapper;

    @Autowired
    RoleMapper roleMapper;


    @Override
    public PageModel findAuthorityByPage(String pageSize, String pageNo, String authorityname, String url) {

        PageModel<Authority> authorityPageModel = new PageModel<>();

        QueryCondition queryCondition = new QueryCondition();

        if (authorityname != null && authorityname != "") {
            queryCondition.setName("%" + authorityname + "%");
        }

        if (url != null && authorityname != "") {
            queryCondition.setUrl("%" + url + "%");
        }


        if (pageSize != null && pageSize != "") {

            authorityPageModel.setPageSize(Integer.parseInt(pageSize));

        } else if (pageNo != null && pageNo != "") {

            authorityPageModel.setPageNo(Integer.parseInt(pageNo));
        }

        Integer totalCount = authorityMapper.totalCountByselectByQuerycondition(queryCondition);
        authorityPageModel.setTotalCount(totalCount);
        List<Authority> authorityList = authorityMapper.selectByQuerycondition(queryCondition, authorityPageModel);
        authorityPageModel.setList(authorityList);
        return authorityPageModel;
    }

    @Override
    public PageModel findRoleByPage(String pageSize, String pageNo, String rolename) {
        PageModel<Role> rolePageModel = new PageModel<>();

        QueryCondition queryCondition = new QueryCondition();

        if (rolename != null && rolename != "") {
            queryCondition.setName("%" + rolename + "%");
        }

        if (pageSize != null && pageSize != "") {

            rolePageModel.setPageSize(Integer.parseInt(pageSize));

        }

        if (pageNo != null && pageNo != "") {

            rolePageModel.setPageNo(Integer.parseInt(pageNo));
        }
        System.out.println(rolePageModel);

        Integer totalCount = roleMapper.totalCountByselectByQuerycondition(queryCondition);
        rolePageModel.setTotalCount(totalCount);
        List<Role> roleList = roleMapper.selectByQuerycondition(queryCondition, rolePageModel);
        rolePageModel.setList(roleList);
        return rolePageModel;
    }


}
