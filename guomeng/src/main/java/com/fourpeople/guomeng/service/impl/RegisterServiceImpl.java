package com.fourpeople.guomeng.service.impl;

import com.fourpeople.guomeng.dao.UserMapper;
import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    UserMapper userMapper;
    @Override
    public String register(String username, String password) {
        User user = new User();
        user.setUserName(username);
        user.setPassWord(password);
        if (userMapper.insert(user)>0){
            return "注册成功";
        }else {
            return "注册失败";
        }
    }
}
