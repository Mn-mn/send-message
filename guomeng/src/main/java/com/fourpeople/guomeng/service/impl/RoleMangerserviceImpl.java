package com.fourpeople.guomeng.service.impl;

import com.fourpeople.guomeng.dao.RoleMapper;
import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.entity.Role;
import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.service.RoleMangerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleMangerserviceImpl implements RoleMangerService {

    @Autowired
    RoleMapper roleMapper;

    @Override
    public List<Role> roleList() {
        return roleMapper.selectAll();
    }

    @Override
    public Role roleByUid(User user) {
        return roleMapper.selectByUid(user);
    }

    @Override
    public List<Role> roleByAid(Authority authority) {
        return roleMapper.selectByAid(authority);
    }

    @Override
    public Integer roleAdd(Role role) {
        return roleMapper.insert(role);
    }

    @Override
    public Integer roleDelete(Role role) {
        return roleMapper.deleteByPrimaryKey(role.getRid());
    }

    @Override
    public Integer roleUpdate(Role role) {
        return roleMapper.updateByPrimaryKey(role);
    }

    @Override
    public List<Role> roleAll() {
        return roleMapper.selectAll();
    }





}
