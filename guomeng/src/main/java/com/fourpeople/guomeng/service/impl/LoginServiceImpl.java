package com.fourpeople.guomeng.service.impl;

import com.fourpeople.guomeng.dao.AuthorityMapper;
import com.fourpeople.guomeng.dao.RoleMapper;
import com.fourpeople.guomeng.dao.UserMapper;
import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.entity.Role;
import com.fourpeople.guomeng.entity.User;
import com.fourpeople.guomeng.util.ResourceListVO;
import com.fourpeople.guomeng.util.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoginServiceImpl implements com.fourpeople.guomeng.service.LoginService {

    @Autowired
    UserMapper userMapper;


    @Autowired
    RoleMapper roleMapper;


    @Autowired
    AuthorityMapper authorityMapper;


    private List<ResourceListVO> resourceListVOS;


    @Override
    public User login(String username, String password) {
        User user = new User();
        user.setUserName(username);
        user.setPassWord(password);
        User loginUser = userMapper.findByUsername(user);
        if (loginUser != null) {
            if (loginUser.getPassWord().equals(password)) {
                return loginUser;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }


    /**
     * 获取账号的资源树
     */
    public List<ResourceListVO> queryMenuTree(String username, String password) {
        User user = new User();
        user.setUserName(username);
        user.setPassWord(password);
        User mapperByUsername = userMapper.findByUsername(user);

        Role roles = roleMapper.selectByUid(mapperByUsername);

        //查询当前用户的所有菜单列表
        List<Authority> authorities = authorityMapper.selectByRid(roles.getRid());
        //集合类型转换，Authority转换成ResourceListVO
        List<ResourceListVO> resourceListVOS = authorities.stream().map(star -> {
            ResourceListVO resourceListVO = new ResourceListVO();
            resourceListVO.setId(star.getAid());
            resourceListVO.setParentid(star.getParentid());
            resourceListVO.setName(star.getAuthorityname());
            resourceListVO.setUrl(star.getUrl());
            return resourceListVO;
        }).collect(Collectors.toList());

        return TreeUtils.generateTrees(resourceListVOS);
    }


}
