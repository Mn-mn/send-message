package com.fourpeople.guomeng.service;

import com.fourpeople.guomeng.model.PageModel;

public interface PageService {

    PageModel findAuthorityByPage(String pageSize,String pageNo,String authorityname,String url);

    PageModel findRoleByPage(String pageSize,String pageNo,String rolename);
}
