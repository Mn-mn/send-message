package com.fourpeople.guomeng.service;

import com.fourpeople.guomeng.entity.User;

public interface LoginService {
    User login(String username, String password);


}
