package com.fourpeople.guomeng.service;

import com.fourpeople.guomeng.entity.Authority;
import com.fourpeople.guomeng.entity.Role;
import com.fourpeople.guomeng.entity.User;

import java.util.List;

public interface RoleMangerService {


    List<Role> roleList();

    Role roleByUid(User user);

    List<Role> roleByAid(Authority authority);

    Integer roleAdd(Role role);

    Integer roleDelete(Role role);

    Integer roleUpdate(Role role);

    List<Role> roleAll();


}
