package com.fourpeople.zhangxiang.model;

import com.fourpeople.zhangxiang.entity.Authority;

import java.io.Serializable;
import java.util.List;

public class UserRoleAuthority implements Serializable {
    private Integer uid;
    private String username;
    private String password;
    private Integer rid;
    private String rolename;
    private List<Authority> authority;

    public List<Authority> getAuthority() {
        return authority;
    }

    public void setAuthority(List<Authority> authority) {
        this.authority = authority;
    }

    public Integer getUid() {
        return uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }


}
