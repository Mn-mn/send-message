package com.fourpeople.zhangxiang;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@EnableOpenApi
@SpringBootApplication
@MapperScan("com.fourpeople.zhangxiang.dao")
public class RBACApplication {
    public static void main(String[] args) {
        SpringApplication.run(RBACApplication.class, args);
    }
}

