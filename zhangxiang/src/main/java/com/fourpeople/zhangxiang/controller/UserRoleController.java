package com.fourpeople.zhangxiang.controller;

import com.fourpeople.zhangxiang.entity.Role;
import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.entity.UserRole;
import com.fourpeople.zhangxiang.model.PageBean;
import com.fourpeople.zhangxiang.service.UserRoleService;
import com.fourpeople.zhangxiang.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "查看，删除，修改，增加用户角色")
@RestController
public class UserRoleController {
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private UserService userService;

    @GetMapping("/showUserRoles")
    @ApiOperation(value = "根据用户id查看当前用户角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户id", required = true),
    })
    public List<Role> findUserRole(Integer uid){
        return userRoleService.findRoleByUid(uid);
    }

    @GetMapping("/showRolesByUsername")
    @ApiOperation(value = "根据用户名字查看当前用户角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户姓名", required = true),
    })
    public List<Role> findRoleByUsername(String username){
        return userRoleService.findRoleByUsername(username);
    }

    @GetMapping("/showUserByRid")
    @ApiOperation(value = "根据用角色id查看用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "rid", value = "角色id", required = true),
    })
    public List<User> findUserByRid(Integer rid){
        return userRoleService.findUserByRid(rid);
    }

    @ApiOperation(value = "删除当前用户角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户id", required = true),
            @ApiImplicitParam(name = "rid", value = "角色id", required = true),
    })
    @PostMapping("/deleteUserRole")
    public String deleteUserRole(Integer uid,Integer rid){
        return (userRoleService.deleteUserRole(uid, rid)) >0 ? "删除成功" : "删除失败";
    }
    @ApiOperation(value = "添加当前用户角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "用户id", required = true),
            @ApiImplicitParam(name = "rid", value = "角色id", required = true),
    })
    @PostMapping("/addUserRole")
    public String addRole(Integer uid,Integer rid){
        return userRoleService.addUserRole(uid,rid) >0 ? "添加角色成功" : "添加角色失败";
    }
    @ApiOperation(value = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currentPage", value = "当前页", required = true),
            @ApiImplicitParam(name = "pageSize", value = "每页的大小", required = true),
    })
    @GetMapping("/showUsersByPage")
    public PageBean<User> showUsersByPage(Integer currentPage, Integer pageSize){
        return userService.pageAllUser(currentPage,pageSize);
    }
    @ApiOperation(value = "默认分页查询")
    @GetMapping("/showUser")
    public PageBean<User> showUsersByPage(){
        return userService.pageAllUser(1,2);
    }

}
