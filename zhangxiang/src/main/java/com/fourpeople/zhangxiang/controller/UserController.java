package com.fourpeople.zhangxiang.controller;

import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Api(tags = "用户登录，注册，注销")
@RestController(value = "/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "登录", notes = "用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true),
            @ApiImplicitParam(name = "password", value = "登陆密码", required = true),
    })
    @PostMapping("/login")
    public String login(String username, String password, HttpSession session, HttpServletRequest request, HttpServletResponse response){
        return userService.login(username.trim(), password.trim(), session, request, response).toJSONString();
    }

    /*@ApiOperation(value = "注销")
    @PostMapping("/loginout")
    public String loginOut(HttpSession session , HttpServletRequest request, HttpServletResponse response){
        // 删除session里面的用户信息
        //session.removeAttribute(Const.SYSTEM_USER_SESSION);
        //session.removeAttribute("user");
        session.invalidate();
        // 保存cookie，实现自动登录
        Cookie cookie_username = new Cookie("cookie_username", "");
        // 设置cookie的持久化时间，0
        cookie_username.setMaxAge(0);
        // 设置为当前项目下都携带这个cookie
        cookie_username.setPath(request.getContextPath());
        // 向客户端发送cookie
        response.addCookie(cookie_username);
        return "login";
    }*/

    @ApiOperation(value = "用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true),
            @ApiImplicitParam(name = "password", value = "登陆密码", required = true),
    })
    @PostMapping("/register")
    public String registerUser(User user){
        return userService.register(user);
    }
}

