package com.fourpeople.zhangxiang.controller;

import com.fourpeople.zhangxiang.entity.Menu;
import com.fourpeople.zhangxiang.entity.User;

import com.fourpeople.zhangxiang.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MenuController {
    @Autowired
    private MenuService menuService;

    @RequestMapping("/showFirstMenu")
    public List<Menu> showFirstMenu(Integer rid){
        return menuService.findFirstMenu(rid);
    }


}
