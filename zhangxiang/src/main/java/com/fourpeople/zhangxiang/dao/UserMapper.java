package com.fourpeople.zhangxiang.dao;

import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.model.PageBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface UserMapper {

    User findByUsername(String username);

    List<User> findByAll();

    List<User> findAllByPage(Integer startIndex,Integer pageSize);

    Integer findCount();

    Integer deleteByUsername(User user);

    Integer deleteByUid(Integer uid);

    Integer updateByUsername(User user);

    Integer updateByUid(User user);

    Integer insertUser(User user);

}
