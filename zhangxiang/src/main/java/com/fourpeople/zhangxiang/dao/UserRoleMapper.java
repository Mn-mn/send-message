package com.fourpeople.zhangxiang.dao;

import com.fourpeople.zhangxiang.entity.Role;
import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserRoleMapper {
    List<Role> findRoleByUid(Integer uid);
    List<Role> findRoleByUsername(String username);
    List<User> findUserByRid(Integer rid);
    Integer deleteRoleByRid(Integer uid,Integer rid);
    Integer updateUserRole(UserRole userRole);
    Integer insertUserRole(UserRole userRole);
}
