package com.fourpeople.zhangxiang.dao;

import com.fourpeople.zhangxiang.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper {
    List<Menu> selectFirstMenuByRid(Integer rid);

}
