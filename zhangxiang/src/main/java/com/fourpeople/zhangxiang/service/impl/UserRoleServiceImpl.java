package com.fourpeople.zhangxiang.service.impl;

import com.fourpeople.zhangxiang.dao.UserRoleMapper;
import com.fourpeople.zhangxiang.entity.Role;
import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.entity.UserRole;
import com.fourpeople.zhangxiang.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public List<Role> findRoleByUid(Integer uid) {
        return userRoleMapper.findRoleByUid(uid);
    }

    @Override
    public List<Role> findRoleByUsername(String username) {
        return userRoleMapper.findRoleByUsername(username);
    }

    @Override
    public List<User> findUserByRid(Integer rid) {
        return userRoleMapper.findUserByRid(rid);
    }

    @Override
    public Integer deleteUserRole(Integer uid,Integer rid) {
        return userRoleMapper.deleteRoleByRid(uid,rid) ;
    }

    @Override
    public Integer addUserRole(Integer uid,Integer rid) {
        UserRole userRole=new UserRole(uid,rid);
        return userRoleMapper.insertUserRole(userRole);
    }
}
