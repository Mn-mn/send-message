package com.fourpeople.zhangxiang.service;

import com.fourpeople.zhangxiang.entity.Role;
import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.entity.UserRole;

import java.util.List;

public interface UserRoleService {
    List<Role> findRoleByUid(Integer uid);
    List<Role> findRoleByUsername(String username);
    List<User> findUserByRid(Integer rid);

    Integer deleteUserRole(Integer uid,Integer rid);
    Integer addUserRole(Integer uid,Integer rid);

}
