package com.fourpeople.zhangxiang.service;

import com.fourpeople.zhangxiang.entity.Menu;

import java.util.List;

public interface MenuService {
    List<Menu> findFirstMenu(Integer rid);

}
