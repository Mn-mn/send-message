package com.fourpeople.zhangxiang.service;

import com.alibaba.fastjson.JSONObject;
import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.model.PageBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public interface UserService {
    JSONObject login(String username, String password, HttpSession session, HttpServletRequest request, HttpServletResponse response);
    String register(User user);

    PageBean<User> pageAllUser(Integer currentPage,Integer pageSize);

}
