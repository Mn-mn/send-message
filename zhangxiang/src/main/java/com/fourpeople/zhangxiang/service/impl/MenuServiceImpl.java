package com.fourpeople.zhangxiang.service.impl;

import com.fourpeople.zhangxiang.dao.MenuMapper;
import com.fourpeople.zhangxiang.entity.Menu;
import com.fourpeople.zhangxiang.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> findFirstMenu(Integer rid) {
        menuMapper.selectFirstMenuByRid(rid);
        return menuMapper.selectFirstMenuByRid(rid);
    }
}
