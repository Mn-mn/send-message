package com.fourpeople.zhangxiang.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.fourpeople.zhangxiang.dao.UserMapper;
import com.fourpeople.zhangxiang.entity.User;
import com.fourpeople.zhangxiang.model.PageBean;
import com.fourpeople.zhangxiang.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public JSONObject login(String username, String password, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        // 最终返回的对象
        JSONObject res = new JSONObject();
        res.put("code", 0);
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            res.put("msg", "请输入用户名或密码");
            return res;
        }
        User user = userMapper.findByUsername(username);
        if(user==null){
            res.put("msg", "该账号不存在，请检查后重试");
            return res;
        }
        // 验证密码是否正确
        if (!password.equals(user.getPassword())) {
            res.put("msg", "手机号或密码错误，请检查后重试");
            return res;
        }
        // 将登录用户信息保存到session中
        session.setAttribute("user",user);
        // 保存cookie，实现自动登录
        Cookie cookie_username = new Cookie("cookie_username", username);
        // 设置cookie的持久化时间，30天
        cookie_username.setMaxAge(30 * 24 * 60 * 60);
        // 设置为当前项目下都携带这个cookie
        cookie_username.setPath(request.getContextPath());
        // 向客户端发送cookie
        response.addCookie(cookie_username);
        res.put("code", 1);
        res.put("msg", "登录成功");
        res.put("user",user);
        return res;
    }

    @Override
    public String register(User user) {
        Integer integer = userMapper.insertUser(user);
        return integer!=0 ? "login" : "注册失败" ;
    }

    @Override
    public PageBean<User> pageAllUser(Integer currentPage, Integer pageSize) {
        PageBean<User> pageBean=new PageBean<>();
        pageBean.setPageSize(pageSize);
        pageBean.setCurrentPage(currentPage);
        Integer startIndex=pageBean.getStartIndex();
        List<User> users = userMapper.findAllByPage(startIndex, pageSize);
        pageBean.setData(users);
        Integer totalCount= userMapper.findCount();
        pageBean.setTotalCount(totalCount);
        return pageBean;
    }

}
